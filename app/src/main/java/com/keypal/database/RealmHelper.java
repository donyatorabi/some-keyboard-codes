package com.keypal.database;

import android.content.Context;

import com.keypal.model.KeyboardTheme;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

public class RealmHelper {

    Realm realm;
    Context context;

    @Inject
    public RealmHelper(Context context) {

        this.context=context;

    }

    public void createRealm(Context c){
        Realm.init(c);

        RealmConfiguration config = new RealmConfiguration.
                Builder().
                deleteRealmIfMigrationNeeded().
                build();
        Realm.setDefaultConfiguration(config);
        realm = Realm.getInstance(config);
    }

    public void save(final KeyboardTheme keyboardTheme) {
        realm.beginTransaction();
        KeyboardTheme k = realm.copyToRealm(keyboardTheme);
        realm.commitTransaction();

    }

    public List<KeyboardTheme> retrieveRequestName() {


        List<KeyboardTheme> request = new ArrayList<>();
        RealmResults<KeyboardTheme> requests = realm.where(KeyboardTheme.class).findAll();

        for (KeyboardTheme k : requests) {

            request.add(k);
        }

        return request;
    }


    public KeyboardTheme retrieve(String id) {


        //KeyboardTheme request = new KeyboardTheme();
        KeyboardTheme requests = realm.where(KeyboardTheme.class).equalTo("id", id).findFirst();


        return requests;
    }

    public void deleteReq(String requestName) {
        realm.beginTransaction();
        RealmResults<KeyboardTheme> rows = realm.where(KeyboardTheme.class).equalTo("backgroundImg", requestName).findAll();
        rows.deleteAllFromRealm();
        realm.commitTransaction();
    }

    public void deleteAll() {

        realm.beginTransaction();
        RealmResults<KeyboardTheme> rows = realm.where(KeyboardTheme.class).findAll();
        rows.deleteAllFromRealm();
        realm.commitTransaction();
    }


}


