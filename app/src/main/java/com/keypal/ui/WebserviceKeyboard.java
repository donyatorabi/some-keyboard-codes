package com.keypal.ui;

public class WebserviceKeyboard {
    public int start, center, end, stroke_color, stroke_width, corner_radius, text_color;
    public String backspace, language, spacebar, enter, emoji, shift;

    public WebserviceKeyboard(int start,int center,int end,int stroke_color,int stroke_width,int corner_radius,int text_color,
                              String backspace,String language,String spacebar,String enter,String emoji,String shift){
        this.start=start;
        this.center=center;
        this.end=end;
        this.stroke_color=stroke_color;
        this.stroke_width=stroke_width;
        this.corner_radius=corner_radius;
        this.text_color=text_color;
        this.backspace=backspace;
        this.language=language;
        this.spacebar=spacebar;
        this.enter=enter;
        this.emoji=emoji;
        this.shift=shift;
    }
}
