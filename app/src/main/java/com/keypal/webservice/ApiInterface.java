package com.keypal.webservice;

import com.keypal.model.KeyboardTheme;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiInterface {

@POST("webservice/keyboard_json.php")
@FormUrlEncoded
Observable<List<KeyboardTheme>> getAllKeyboards(@Field("keyword") int keyword);

@POST("webservice/search.php")
@FormUrlEncoded
Observable<List<KeyboardTheme>> getSliderKeyboards(@Field("key_id") int key_id, @Field("theme_id") int theme_id);

}
