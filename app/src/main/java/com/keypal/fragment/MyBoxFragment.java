package com.keypal.fragment;

import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.keypal.R;
import com.keypal.adapter.MyBoxAdapter;
import com.keypal.database.RealmHelper;
import com.keypal.di.DaggerDatabaseComponent;
import com.keypal.di.DatabaseComponent;
import com.keypal.di.DatabaseModule;
import com.keypal.model.KeyboardTheme;

import java.util.List;

import javax.inject.Inject;

public class MyBoxFragment extends Fragment {

    @Inject
    RealmHelper realmHelper;
    RecyclerView rv;
    DatabaseComponent databaseComponent;

    List<KeyboardTheme> keyboardThemes;

    MyBoxAdapter adapter;

    Context con;

    public MyBoxFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_my_box, container, false);
        con=getActivity();
        // Inflate the layout for this fragment
        rv = (RecyclerView) view.findViewById(R.id.rv);

        rv.setLayoutManager(new GridLayoutManager(getActivity(),1));
        rv.setAdapter(adapter);

        databaseComponent=DaggerDatabaseComponent.builder().databaseModule(new DatabaseModule((Application) con.getApplicationContext())).build();
        databaseComponent.inMyBoxFragmet(this);
        databaseComponent.getRealmHelper().createRealm(con);

        keyboardThemes = databaseComponent.getRealmHelper().retrieveRequestName();
        adapter = new MyBoxAdapter(con, keyboardThemes,(Application) con.getApplicationContext());
        rv.setAdapter(adapter);
        return view;
    }

}
