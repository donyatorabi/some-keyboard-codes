package com.keypal.adapter;

import android.view.View;

public interface ItemClickListener {
    public void onItemClick(View v, int position);
}