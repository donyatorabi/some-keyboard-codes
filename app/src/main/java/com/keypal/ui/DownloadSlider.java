package com.keypal.ui;

import android.app.Application;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.keypal.Directory.DirectoryHelper;
import com.keypal.R;
import com.keypal.database.RealmHelper;
import com.keypal.di.DaggerDatabaseComponent;
import com.keypal.di.DatabaseComponent;
import com.keypal.di.DatabaseModule;
import com.keypal.model.KeyboardTheme;
import com.keypal.service.DownloadService;
import com.keypal.util.SaveSharePref;
import com.keypal.webservice.ApiClient;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

public class DownloadSlider extends Dialog {
    SubKeyboardView subKeyboardView;
    Context c;
    Canvas canvas;
    public Dialog d;
    public ImageView close_alert,keyboard_alert;
    Button download;
    Uri uri, uriBackground, uriBackspace, uriLanguage, uriSpace_bar, uriEnter, uriEmoji, uriShift;
    static List<KeyboardTheme> keyboardThemeList;
    DatabaseComponent databaseComponent;

    String des;
    static WebserviceKeyboard webserviceKeyboard;
    SaveSharePref saveSharePref;
    LottieAnimationView lottieAnimationView;
    KeyboardTheme keyboardTheme;

    @Inject
    RealmHelper realmHelper;
    private boolean are;

    public DownloadSlider(Context context, List<KeyboardTheme> keyboardThemeList) {
        super(context);
        this.c=context;
        this.keyboardThemeList=keyboardThemeList;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_dialog);

        lottieAnimationView=findViewById(R.id.lottie_loading);

        des=DirectoryHelper.ROOT_DIRECTORY_NAME.concat("/");

        close_alert=findViewById(R.id.close_alert);
        keyboard_alert=findViewById(R.id.keyboard_alert);
        download=findViewById(R.id.download);

        keyboardTheme=keyboardThemeList.get(0);
        uri=Uri.parse(ApiClient.BaseUrl+ "images/" +keyboardTheme.getScreenshotImg());
        uriBackground=Uri.parse(ApiClient.BaseUrl+ "images/" +keyboardTheme.getBackgroundImg());
        uriBackspace=Uri.parse(ApiClient.BaseUrl+ "key_images/" +keyboardTheme.getBackspace());
        uriLanguage=Uri.parse(ApiClient.BaseUrl+ "key_images/" +keyboardTheme.getLanguage());
        uriSpace_bar=Uri.parse(ApiClient.BaseUrl+ "key_images/" +keyboardTheme.getSpaceBar());
        uriEnter=Uri.parse(ApiClient.BaseUrl+ "key_images/" +keyboardTheme.getEnter());
        uriEmoji=Uri.parse(ApiClient.BaseUrl+ "key_images/" +keyboardTheme.getEmoji());
        uriShift=Uri.parse(ApiClient.BaseUrl+ "key_images/" +keyboardTheme.getShift());

        Glide.with(c)
                .load(uri)
                .placeholder(R.drawable.backgrounf)
                .into(keyboard_alert);

        databaseComponent=DaggerDatabaseComponent.builder().databaseModule(new DatabaseModule((Application) c.getApplicationContext())).build();
        databaseComponent.inDownloadSlider(this);
        databaseComponent.getRealmHelper().createRealm(c);

        List<KeyboardTheme> k=databaseComponent.getRealmHelper().retrieveRequestName();

/*
            String pathname=des + keyboardTheme.getScreenshotImg();
*/

        File screenshotFile =  new File(Environment.getExternalStorageDirectory() + "/" +".keypal"+ "/" + keyboardTheme.getScreenshotImg());

        File backgroundFile =  new File(Environment.getExternalStorageDirectory() + "/" +".keypal"+ "/" + keyboardTheme.getBackgroundImg());

        if(screenshotFile.isFile() && backgroundFile.isFile()){
            lottieAnimationView.setVisibility(View.GONE);
            download.setText(c.getString(R.string.apply));
        }
        else{
            download.setText(c.getString(R.string.download));
        }


        /*if(databaseComponent.getRealmHelper().retrieveRequestName().equals(keyboardTheme)){
            download.setText("Apply");
        }
        else{
            download.setText("Download");
        }*/

        close_alert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });



        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(download.getText().equals(c.getString(R.string.download))) {
                    lottieAnimationView.setVisibility(View.VISIBLE);

                    Intent i = new Intent(DownloadService.getDownloadService(c, String.valueOf(uri), String.valueOf(uriBackground),String.valueOf(uriBackspace),
                            String.valueOf(uriLanguage),String.valueOf(uriSpace_bar),String.valueOf(uriEnter),String.valueOf(uriEmoji),String.valueOf(uriShift)));
                    i.putExtra("screenshot", keyboardTheme.getScreenshotImg());
                    i.putExtra("background", keyboardTheme.getBackgroundImg());
                    i.putExtra("backspace", keyboardTheme.getBackspace());
                    i.putExtra("language", keyboardTheme.getLanguage());
                    i.putExtra("spacebar", keyboardTheme.getSpaceBar());
                    i.putExtra("enter", keyboardTheme.getEnter());
                    i.putExtra("emoji", keyboardTheme.getEmoji());
                    i.putExtra("shift", keyboardTheme.getShift());
                    i.putExtra("id", keyboardTheme.getId());
                    i.putExtra("key_id", keyboardTheme.getKeyId());
                    i.putExtra("start_color", keyboardTheme.getStartColor());
                    i.putExtra("center_color", keyboardTheme.getCenterColor());
                    i.putExtra("end_color", keyboardTheme.getEndColor());
                    i.putExtra("stroke_width", keyboardTheme.getStrokeWidth());
                    i.putExtra("stroke_color", keyboardTheme.getStrokeColor());
                    i.putExtra("corner_radius", keyboardTheme.getCornerRadius());
                    i.putExtra("text_color", keyboardTheme.getTextColor());
                    i.putExtra("des", des);
                    c.startService(i);
                        /*final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                            }
                        }, 3000);*/
                }
                else{
                    //lottieAnimationView.setVisibility(View.GONE);
                    KeyboardTheme keyboard=databaseComponent.getRealmHelper().retrieve(keyboardTheme.getId());
                        /*webserviceKeyboard=new WebserviceKeyboard(Color.parseColor(keyboard.getStartColor()),Color.parseColor(keyboard.getCenterColor()),
                                Color.parseColor(keyboard.getEndColor()), Color.parseColor(keyboard.getStrokeColor()), Integer.parseInt(keyboard.getStrokeWidth()),
                                Integer.parseInt(keyboard.getCornerRadius()), Color.parseColor(keyboard.getTextColor()),keyboard.getBackspace(),keyboard.getLanguage(),
                                keyboard.getSpaceBar(),keyboard.getEnter(),keyboard.getEmoji(),keyboard.getShift());*/
                    saveSharePref=new SaveSharePref(c);
                    saveSharePref.setStr("id", keyboard.getId());
                    saveSharePref.setInt("start",Color.parseColor(keyboard.getStartColor()));
                    saveSharePref.setInt("center",Color.parseColor(keyboard.getCenterColor()));
                    saveSharePref.setInt("end",Color.parseColor(keyboard.getEndColor()));
                    saveSharePref.setInt("stroke_color",Color.parseColor(keyboard.getStrokeColor()));
                    saveSharePref.setInt("stroke_width",Integer.parseInt(keyboard.getStrokeWidth()));
                    saveSharePref.setInt("corner_radius",Integer.parseInt(keyboard.getCornerRadius()));
                    saveSharePref.setInt("text_color",Color.parseColor(keyboard.getTextColor()));
                    saveSharePref.setStr("background_image",keyboard.getBackgroundImg());
                    saveSharePref.setStr("backspace",keyboard.getBackspace());
                    saveSharePref.setStr("language",keyboard.getLanguage());
                    saveSharePref.setStr("spacebar",keyboard.getSpaceBar());
                    saveSharePref.setStr("enter",keyboard.getEnter());
                    saveSharePref.setStr("emoji",keyboard.getEmoji());
                    saveSharePref.setStr("shift",keyboard.getShift());

                }
                lottieAnimationView.setVisibility(View.GONE);
                Toast.makeText(c, "done!", Toast.LENGTH_LONG).show();
                download.setText(c.getString(R.string.apply));

            }
        });



        Window window = getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.BOTTOM);
    }
   /* public static String setKeyboard(){

        return keyboardTheme.getId();
    }*/


}
