package com.keypal.di;

import com.keypal.MainActivity;
import com.keypal.adapter.MyBoxAdapter;
import com.keypal.database.RealmHelper;
import com.keypal.fragment.MyBoxFragment;
import com.keypal.service.DownloadService;
import com.keypal.service.MyInputMethodService;
import com.keypal.ui.DownloadDialog;
import com.keypal.ui.DownloadSlider;
import com.keypal.ui.SubKeyboardView;

import dagger.Component;

@DatabaseScope
@Component(modules = {DatabaseModule.class})
public interface DatabaseComponent {
    void inService(DownloadService downloadService);
    void inMyBoxFragmet(MyBoxFragment myBoxFragment);
    void inMyBoxAdapter(MyBoxAdapter myBoxAdapter);
    void inDownload(DownloadDialog downloadDialog);
    void inDownloadSlider(DownloadSlider downloadDialog);
    void inSubKeyboardView(SubKeyboardView subKeyboardView);
    void inMyInputMethodService(MyInputMethodService myInputMethodService);
    RealmHelper getRealmHelper();

}