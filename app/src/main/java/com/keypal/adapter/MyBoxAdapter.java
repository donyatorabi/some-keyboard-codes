package com.keypal.adapter;

import android.app.Application;
import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.keypal.R;
import com.keypal.database.RealmHelper;
import com.keypal.di.DaggerDatabaseComponent;
import com.keypal.di.DatabaseComponent;
import com.keypal.di.DatabaseModule;
import com.keypal.model.KeyboardTheme;
import com.keypal.ui.DownloadDialog;
import com.keypal.webservice.ApiClient;

import java.util.List;

import javax.inject.Inject;

public class MyBoxAdapter extends RecyclerView.Adapter<MyBoxViewHolder> {

    Context c;
    List<KeyboardTheme> keyboardThemes;
    Uri uri, uriBackground;

    @Inject
    RealmHelper realmHelper;

    DatabaseComponent databaseComponent;

    public MyBoxAdapter(Context c, List<KeyboardTheme> keyboardThemes, Application application){
        this.c=c;
        this.keyboardThemes=keyboardThemes;

        databaseComponent=DaggerDatabaseComponent.builder().databaseModule(new DatabaseModule(application)).build();
        databaseComponent.inMyBoxAdapter(this);
        databaseComponent.getRealmHelper().createRealm(c);
    }

    @NonNull
    @Override
    public MyBoxViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v=LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.keyboard_row,null);

        return new MyBoxViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyBoxViewHolder myBoxViewHolder, int i) {
        KeyboardTheme keyboardTheme=keyboardThemes.get(i);
        uri=Uri.parse(ApiClient.BaseUrl+ "images/" +keyboardTheme.getScreenshotImg());
        //uriBackground=Uri.parse(ApiClient.BaseUrl+ "images/" +keyboardTheme.getScreenshotImg());

        myBoxViewHolder.name.setText(keyboardTheme.getTitle());
        myBoxViewHolder.image_view_loading.setVisibility(View.GONE);
        Glide.with(c)
                .load(uri)
                .into(myBoxViewHolder.image_view);
        myBoxViewHolder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                DownloadDialog cdd=new DownloadDialog(c,keyboardThemes, position);
                cdd.show();
            }
        });
        myBoxViewHolder.image_view.setVisibility(View.VISIBLE);    }

    @Override
    public int getItemCount() {
        return keyboardThemes.size();
    }
}
