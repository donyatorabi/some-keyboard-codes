package com.keypal.fragment;

import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.keypal.MainActivity;
import com.keypal.R;
import com.keypal.adapter.RecyclerAdapter;
import com.keypal.model.IMessageListener;
import com.keypal.ui.Slider;
import com.keypal.util.ConnectionDetector;
import com.keypal.webservice.WebserviceCaller;

import java.util.HashMap;
import java.util.List;

public class HomeFragment extends Fragment implements ViewPagerEx.OnPageChangeListener, BaseSliderView.OnSliderClickListener {

    RecyclerView rv;
    View view;
    RecyclerAdapter adapter;
    WebserviceCaller webserviceCaller;
    private SliderLayout mDemoSlider;
    private AdView mAdView;

    String url="http://key.androidsupport.ir/images/Screenshot_20190528-120119.png";
    String url2="http://key.androidsupport.ir/images/Screenshot_20190528-115656.png";
    String url3="http://key.androidsupport.ir/images/Screenshot_20190528-113827.png";

    TextView not_connected;
    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view=inflater.inflate(R.layout.fragment_home, container, false);

        ConnectionDetector connectionDetector=new ConnectionDetector(getActivity());
        not_connected=view.findViewById(R.id.not_connected);
        if(connectionDetector.isConnected()){
            not_connected.setVisibility(View.GONE);
        }
        else {
            not_connected.setVisibility(View.VISIBLE);
        }
        MobileAds.initialize(getActivity(),
                "ca-app-pub-5166147620362120~4731536717");

        mAdView = view.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
                //Log.e("","");
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
                //Log.e("error", String.valueOf(errorCode));
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
                //Log.e("","");
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
                //Log.e("","");
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
                //Log.e("","");
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
                //Log.e("","");
            }
        });
        rv = (RecyclerView) view.findViewById(R.id.rv);
        rv.setNestedScrollingEnabled(false);
        webserviceCaller=new WebserviceCaller();
        webserviceCaller.getPosts(0, new IMessageListener() {
            @Override
            public void onSuccessObject(Object response) {
                Log.e("","");
            }

            @Override
            public void onSuccess(List response) {
                adapter = new RecyclerAdapter(getActivity(),response);

                rv.setLayoutManager(new GridLayoutManager(getActivity(),1));
                rv.setAdapter(adapter);

                Log.e("","");
            }

            @Override
            public void onError(String errorMessage) {
                Log.e("","");

            }
        });
        mDemoSlider = (SliderLayout)view.findViewById(R.id.slider);
        Slider slider=new Slider();
        slider.addSlider(getActivity(),mDemoSlider);
/*
        TextSliderView textSliderView = new TextSliderView(getActivity());
        TextSliderView textSliderView2 = new TextSliderView(getActivity());
        TextSliderView textSliderView3 = new TextSliderView(getActivity());
        textSliderView
                .setScaleType(BaseSliderView.ScaleType.CenterCrop)
                .image(url);
        textSliderView2
                .setScaleType(BaseSliderView.ScaleType.CenterCrop)
                .image(url2);
        textSliderView3
                .setScaleType(BaseSliderView.ScaleType.CenterCrop)
                .image(url3);

        mDemoSlider.addSlider(textSliderView);
        mDemoSlider.addSlider(textSliderView2);
        mDemoSlider.addSlider(textSliderView3);*/
        return view;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }
}
