package com.keypal.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class KeyboardTheme extends RealmObject {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("key_id")
    @Expose
    private String keyId;
    @SerializedName("screenshot_img")
    @Expose
    private String screenshotImg;
    @SerializedName("background_img")
    @Expose
    private String backgroundImg;
    @SerializedName("start_color")
    @Expose
    private String startColor;
    @SerializedName("center_color")
    @Expose
    private String centerColor;
    @SerializedName("end_color")
    @Expose
    private String endColor;
    @SerializedName("stroke_width")
    @Expose
    private String strokeWidth;
    @SerializedName("stroke_color")
    @Expose
    private String strokeColor;
    @SerializedName("corner_radius")
    @Expose
    private String cornerRadius;
    @SerializedName("text_color")
    @Expose
    private String textColor;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("backspace")
    @Expose
    private String backspace;
    @SerializedName("language")
    @Expose
    private String language;
    @SerializedName("space_bar")
    @Expose
    private String spaceBar;
    @SerializedName("enter")
    @Expose
    private String enter;
    @SerializedName("emoji")
    @Expose
    private String emoji;
    @SerializedName("shift")
    @Expose
    private String shift;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKeyId() {
        return keyId;
    }

    public void setKeyId(String keyId) {
        this.keyId = keyId;
    }

    public String getScreenshotImg() {
        return screenshotImg;
    }

    public void setScreenshotImg(String screenshotImg) {
        this.screenshotImg = screenshotImg;
    }

    public String getBackgroundImg() {
        return backgroundImg;
    }

    public void setBackgroundImg(String backgroundImg) {
        this.backgroundImg = backgroundImg;
    }

    public String getStartColor() {
        return startColor;
    }

    public void setStartColor(String startColor) {
        this.startColor = startColor;
    }

    public String getCenterColor() {
        return centerColor;
    }

    public void setCenterColor(String centerColor) {
        this.centerColor = centerColor;
    }

    public String getEndColor() {
        return endColor;
    }

    public void setEndColor(String endColor) {
        this.endColor = endColor;
    }

    public String getStrokeWidth() {
        return strokeWidth;
    }

    public void setStrokeWidth(String strokeWidth) {
        this.strokeWidth = strokeWidth;
    }

    public String getStrokeColor() {
        return strokeColor;
    }

    public void setStrokeColor(String strokeColor) {
        this.strokeColor = strokeColor;
    }

    public String getCornerRadius() {
        return cornerRadius;
    }

    public void setCornerRadius(String cornerRadius) {
        this.cornerRadius = cornerRadius;
    }

    public String getTextColor() {
        return textColor;
    }

    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBackspace() {
        return backspace;
    }

    public void setBackspace(String backspace) {
        this.backspace = backspace;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getSpaceBar() {
        return spaceBar;
    }

    public void setSpaceBar(String spaceBar) {
        this.spaceBar = spaceBar;
    }

    public String getEnter() {
        return enter;
    }

    public void setEnter(String enter) {
        this.enter = enter;
    }

    public String getEmoji() {
        return emoji;
    }

    public void setEmoji(String emoji) {
        this.emoji = emoji;
    }

    public String getShift() {
        return shift;
    }

    public void setShift(String shift) {
        this.shift = shift;
    }
}
