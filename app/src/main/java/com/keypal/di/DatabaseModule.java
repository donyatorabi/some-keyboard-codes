package com.keypal.di;

import android.app.Application;
import android.content.Context;

import com.keypal.database.RealmHelper;
import com.keypal.ui.DownloadDialog;

import dagger.Module;
import dagger.Provides;

@Module
public class DatabaseModule {
    private Application application;

    public DatabaseModule(Application application) {
        this.application = application;
    }

    /*@Provides
    @DatabaseScope
    Context DatabaseModule(DownloadDialog downloadDialog) { return application;
    }*/

    @Provides
    @DatabaseScope
    Context provideContext() {
        return application;
    }

    @Provides
    @DatabaseScope
    RealmHelper provideRealm(Context context){
        return new RealmHelper(context);
    }
}
