package com.keypal.webservice;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.keypal.model.IMessageListener;
import com.keypal.model.KeyboardTheme;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class WebserviceCaller {
    ApiInterface apiInterface;

    Context context;

    public WebserviceCaller(){
        apiInterface=ApiClient.getClient().create(ApiInterface.class);
    }


    public void getPosts(int keyword, final IMessageListener iMessageListener){

        Observable<List<KeyboardTheme>> getData=apiInterface.getAllKeyboards(keyword);

        getData.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<KeyboardTheme>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.e("","");
                    }

                    @Override
                    public void onNext(List<KeyboardTheme> keyboardThemes) {
                        Log.e("","");
                        iMessageListener.onSuccess(keyboardThemes);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("","");
                        iMessageListener.onError(e.getMessage().toString());
                    }

                    @Override
                    public void onComplete() {
                        Log.e("","");
                    }
                });
    }

    public void getSlider(int key_id,int theme_id, final IMessageListener iMessageListener){

        Observable<List<KeyboardTheme>> getData=apiInterface.getSliderKeyboards(key_id,theme_id);

        getData.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<KeyboardTheme>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.e("","");
                    }

                    @Override
                    public void onNext(List<KeyboardTheme> keyboardThemes) {
                        Log.e("","");
                        iMessageListener.onSuccess(keyboardThemes);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("","");
                        iMessageListener.onError(e.getMessage().toString());
                    }

                    @Override
                    public void onComplete() {
                        Log.e("","");
                    }
                });
    }

}