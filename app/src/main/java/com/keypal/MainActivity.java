package com.keypal;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.keypal.fragment.HomeFragment;
import com.keypal.fragment.MyBoxFragment;
import com.keypal.util.SaveSharePref;
import com.onesignal.OneSignal;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity {

    RecyclerView rv;
    Toolbar toolbar;
    BottomNavigationView navigationView;
    AlertDialog dialog;
    SaveSharePref saveSharePref;
    static Context context;
    private static final int WRITE_EXTERNAL_STORAGE_REQUEST_CODE = 54654;

    public static void changeToolbarFont(Toolbar toolbar, Activity context) {
        for (int i = 0; i < toolbar.getChildCount(); i++) {
            View view = toolbar.getChildAt(i);
            if (view instanceof TextView) {
                TextView tv = (TextView) view;
                if (tv.getText().equals(toolbar.getTitle())) {
                    applyFont(tv, context);
                    break;
                }
            }
        }
    }

    @SuppressLint("ResourceAsColor")
    public static void applyFont(TextView tv, Activity context) {
        //tv.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/queeenn.otff"));
        tv.setTextColor(Color.WHITE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context=getApplicationContext();
        OneSignal.startInit(context)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();


        saveSharePref=new SaveSharePref(getApplicationContext());
        if(!saveSharePref.getBool("active")) {
            saveSharePref.setStr("id", "6");
            saveSharePref.setInt("start", Color.parseColor("#99FFB6FF"));
            saveSharePref.setInt("center", Color.parseColor("#99C8EDFF"));
            saveSharePref.setInt("end", Color.parseColor("#99FFFFFF"));
            saveSharePref.setInt("stroke_color", Color.parseColor("#000000"));
            saveSharePref.setInt("stroke_width", Integer.parseInt("4"));
            saveSharePref.setInt("corner_radius", Integer.parseInt("50"));
            saveSharePref.setInt("text_color", Color.parseColor("#000000"));
            saveSharePref.setStr("backspace", String.valueOf(R.drawable.ic_backspace_black_24dp));
            saveSharePref.setStr("language", String.valueOf(R.drawable.ic_language_black_24dp));
            saveSharePref.setStr("spacebar", String.valueOf(R.drawable.ic_space_bar_black_24dp));
            saveSharePref.setStr("enter", String.valueOf(R.drawable.ic_keyboard_return_black_24dp));
            saveSharePref.setStr("emoji", String.valueOf(R.drawable.ic_insert_emoticon_black_24dp));
            saveSharePref.setStr("shift", String.valueOf(R.drawable.ic_caps_lock_button));
            saveSharePref.setStr("background_img", String.valueOf(R.drawable.ocean_3605547_960_720));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.System.canWrite(this)) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE}, 2909);
            }
        }
        toolbar = findViewById(R.id.toolbar);
        toolbar.setLogo(getResources().getDrawable(R.mipmap.launcher_keyboard_round));
        setSupportActionBar(toolbar);
        changeToolbarFont(toolbar, this);

        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        if (netInfo == null) {
            builder.setMessage("You need internet connection for this app. Please turn on mobile network or Wi-Fi in Settings.")
                    .setTitle("Unable to connect")
                    .setCancelable(false)
                    .setPositiveButton("Settings",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Intent i = new Intent(Settings.ACTION_WIFI_SETTINGS);
                                    startActivity(i);
                                }
                            }
                    )
                    .setNegativeButton("Cancel",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    MainActivity.this.finish();
                                }
                            }
                    );
            AlertDialog alert = builder.create();
            alert.show();
        }

        getSupportFragmentManager().beginTransaction().replace(R.id.container, new HomeFragment()).commit();

        Window window = this.getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        navigationView = findViewById(R.id.bottom_navigation);
        navigationView.setItemIconTintList(null);

        navigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {

                    case R.id.item_home:

                        getSupportFragmentManager().beginTransaction().replace(R.id.container, new HomeFragment()).commit();

                        return true;

                    case R.id.item_mybox:

                        getSupportFragmentManager().beginTransaction().replace(R.id.container, new MyBoxFragment()).commit();

                        return true;

                }

                return false;
            }
        });


// finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.black));
        }

        InputMethodManager imeManager = (InputMethodManager) getApplicationContext().getSystemService(INPUT_METHOD_SERVICE);
        imeManager.showInputMethodPicker();
    }

    boolean doubleBackToExitPressedOnce = false;





    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;

        Toast.makeText(getApplicationContext(), "Press Again to Exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 2909: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Log.e("Permission", "Granted");
                }
                return;
            }
        }
    }
}
