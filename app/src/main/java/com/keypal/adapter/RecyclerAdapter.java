package com.keypal.adapter;

import android.app.Application;
import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.keypal.R;
import com.keypal.model.KeyboardTheme;
import com.keypal.ui.DownloadDialog;
import com.keypal.webservice.ApiClient;

import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerViewHolder> {
    List<KeyboardTheme> keyboardThemeList;
   KeyboardTheme keyboardTheme;
   Uri uri, uriBackground;


    Context context;
    public RecyclerAdapter(Context context, List<KeyboardTheme> keyboardThemes){

        keyboardThemeList=keyboardThemes;
        this.context=context;

    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view=LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.keyboard_row,null);

        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder recyclerViewHolder, final int i) {


        keyboardTheme=keyboardThemeList.get(i);
        uri=Uri.parse(ApiClient.BaseUrl+ "images/" +keyboardTheme.getScreenshotImg());

        recyclerViewHolder.name.setText(keyboardTheme.getTitle());
        recyclerViewHolder.image_view_loading.setVisibility(View.GONE);
        Glide.with(context)
                .load(uri)
                .into(recyclerViewHolder.image_view);
        recyclerViewHolder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                DownloadDialog cdd=new DownloadDialog(context,keyboardThemeList, position);
                cdd.show();
            }
        });
        recyclerViewHolder.image_view.setVisibility(View.VISIBLE);

        /*recyclerViewHolder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                keyboardTheme=keyboardThemeList.get(position);

                Toast.makeText(context,"heloo",Toast.LENGTH_SHORT).show();
               *//* DownloadDialog cdd=new DownloadDialog(context);
                cdd.show();*//*
                *//*try{

                }catch (Exception e){

                }*//*
            }
        });*/

    }

    @Override
    public int getItemCount() {
        return keyboardThemeList.size();
    }

}
