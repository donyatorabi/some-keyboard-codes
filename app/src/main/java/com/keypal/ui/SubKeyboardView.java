package com.keypal.ui;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.inputmethodservice.Keyboard;
import android.os.Environment;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;

import com.keypal.R;
import com.keypal.di.DatabaseComponent;
import com.keypal.util.SaveSharePref;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;

public class SubKeyboardView extends android.inputmethodservice.KeyboardView {
    Drawable drawable, drawableXml;
    XmlResourceParser parser;
    Resources res;
    Context context;
    Boolean caps = false;
    int densityDpi;
    DisplayMetrics metrics;
    int start;
    int center;
    int end;
    int stroke_color;
    DatabaseComponent databaseComponent;
    DownloadDialog downloadDialog;
    SaveSharePref saveSharePref;

    public SubKeyboardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }


    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        res = getResources();

        /*databaseComponent=DaggerDatabaseComponent.builder().databaseModule(new DatabaseModule((Application) context.getApplicationContext())).build();
        databaseComponent.inSubKeyboardView(this);
        databaseComponent.getRealmHelper().createRealm(context);

        KeyboardTheme k=databaseComponent.getRealmHelper().retrieve(id);*/
        saveSharePref = new SaveSharePref(context);

        start = saveSharePref.getInt("start");
        center = saveSharePref.getInt("center");
        end = saveSharePref.getInt("end");
        stroke_color = saveSharePref.getInt("stroke_color");
        int stroke_width = saveSharePref.getInt("stroke_width");
        int corner_radius = saveSharePref.getInt("corner_radius");
        int text_color = saveSharePref.getInt("text_color");
        String backspace = Environment.getExternalStorageDirectory() + "/" + ".keypal" + "/" + saveSharePref.getStr("backspace");
        String language = Environment.getExternalStorageDirectory() + "/" + ".keypal" + "/" + saveSharePref.getStr("language");
        String space_bar = Environment.getExternalStorageDirectory() + "/" + ".keypal" + "/" + saveSharePref.getStr("spacebar");
        String enter = Environment.getExternalStorageDirectory() + "/" + ".keypal" + "/" + saveSharePref.getStr("enter");
        String emoji = Environment.getExternalStorageDirectory() + "/" + ".keypal" + "/" + saveSharePref.getStr("emoji");
        String shift = Environment.getExternalStorageDirectory() + "/" + ".keypal" + "/" + saveSharePref.getStr("shift");

        metrics = getResources().getDisplayMetrics();
        densityDpi = (int) (metrics.density * 160f);
        getKeyboard().setShifted(caps);
        List<Keyboard.Key> keys = getKeyboard().getKeys();
        for (Keyboard.Key key : keys) {
            Log.e("KEY", "Drawing key with code " + key.codes[0]);
            // GradientView vDrawable = new GradientView(R.color.black_o,Color.RED,Color.LTGRAY,2,Color.RED,50);
            GradientView vDrawable = new GradientView(start, center, end, stroke_width, stroke_color, corner_radius);
            int color = getResources().getColor(R.color.white);

            //Drawable dr = context.getResources().getDrawable(R.drawable.round_red_key);
            //Drawable dr = Resources.getSystem().getDrawable(R.drawable.round_red_key);
            //ColorDrawable fd=new ColorDrawable(Color.TRANSPARENT);
            //fd.setBounds(key.x, key.y, key.x + key.width, key.y + key.height);
            //fd.draw(canvas);
            vDrawable.setBounds(key.x, key.y, key.x + key.width, key.y + key.height);
            vDrawable.draw(canvas);
            Paint paint = new Paint();
            paint.setTextAlign(Paint.Align.CENTER);
            paint.setTextSize(Float.parseFloat(getResources().getString(R.string.text_size)));
            paint.setColor(text_color);

            if (key.label != null) {
                canvas.drawText(key.label.toString(), key.x + (key.width / 2),
                        key.y + (key.height / 2), paint);
            } /*else {
                key.icon.setBounds(key.x + (int) (0.3 * key.width), key.y + (int) (0.328
                        * key.height), key.x + (int) (0.7 * key.width), key.y + (int) (0.672
                        * key.height));
                *//*if(key.)){
                    key.icon=null;
                    key.icon=getResources().getDrawable(R.drawable.ic_backspace_black_24dp);
                }*//*
                key.icon.draw(canvas);
        }*/
            if (key.codes[0] == -5) {
                drawKeyBackground(backspace, canvas, key, densityDpi);
            } else if (key.codes[0] == -101) {
                drawKeyBackground(language, canvas, key, densityDpi);
            } else if (key.codes[0] == 32) {
                drawKeyBackground(space_bar, canvas, key, densityDpi);
            } else if (key.codes[0] == -4) {
                drawKeyBackground(enter, canvas, key, densityDpi);
            } else if (key.codes[0] == -102) {
                drawKeyBackground(emoji, canvas, key, densityDpi);
            } else if (key.codes[0] == -1) {
                drawKeyBackground(shift, canvas, key, densityDpi);
            }
        }
    }

    private void drawKeyBackground(String drawableId , Canvas canvas, Keyboard.Key key,int density) {

        File file = new File(drawableId);
        FileInputStream inStream = null;
        try {
            inStream = new FileInputStream(file);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Bitmap bitmap = BitmapFactory.decodeStream(inStream);

        Drawable drawable = new BitmapDrawable(getResources(),bitmap);


        //Bitmap bitmap=BitmapFactory.decodeFile(drawableId);
        int[] drawableState = key.getCurrentDrawableState();
        if (key.codes[0] != 0) {
            drawableState=drawable.getState();
        }
        if(density<650) {
            drawable.setBounds(key.x + (int) (0.23 * key.width), key.y + (int) (0.328
                    * key.height), key.x + (int) (0.77 * key.width), key.y + (int) (0.672
                    * key.height));
        }
        else if(density>=650){
            drawable.setBounds(key.x + (int) (0.35 * key.width), key.y + (int) (0.328
                    * key.height), key.x + (int) (0.65 * key.width), key.y + (int) (0.672
                    * key.height));
        }
        drawable.draw(canvas);
    }

}