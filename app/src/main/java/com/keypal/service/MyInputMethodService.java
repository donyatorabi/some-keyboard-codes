package com.keypal.service;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Application;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.inputmethodservice.InputMethodService;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.ExtractedText;
import android.view.inputmethod.ExtractedTextRequest;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.keypal.R;
import com.keypal.di.DaggerDatabaseComponent;
import com.keypal.di.DatabaseComponent;
import com.keypal.di.DatabaseModule;
import com.keypal.model.KeyboardTheme;
import com.keypal.ui.DownloadDialog;
import com.keypal.util.SaveSharePref;

import java.io.File;

import github.ankushsachdeva.emojicon.EmojiconEditText;
import github.ankushsachdeva.emojicon.EmojiconGridView;
import github.ankushsachdeva.emojicon.EmojiconTextView;
import github.ankushsachdeva.emojicon.EmojiconsPopup;
import github.ankushsachdeva.emojicon.emoji.Emojicon;


public class MyInputMethodService extends InputMethodService implements KeyboardView.OnKeyboardActionListener {

    private KeyboardView keyboardView;
    private Keyboard keyboard;
    ImageView imageView;
    private boolean caps = false;
    private boolean mode = false;
    private boolean modeLanguage = false;
    private boolean modeSym = false;
    EmojiconsPopup popup=null;
    Emojicon emojicon;
    DocumentsContract.Root root;
    private EmojiconsPopup popupWindow;
    private View mPopupView;
    InputMethodManager inputManager;
    DatabaseComponent databaseComponent;
    SaveSharePref saveSharePref;
    String image;


    @Override
    public void onStartInputView(EditorInfo info, boolean restarting) {
        super.onStartInputView(info, restarting);
        setInputView(onCreateInputView());
    }

    @Override
    public View onCreateInputView() {
        keyboardView = (KeyboardView) getLayoutInflater().inflate(R.layout.keyboard_view, null);

        saveSharePref=new SaveSharePref(getApplicationContext());
        String id=saveSharePref.getStr("id");
        String image=saveSharePref.getStr("background_img");

        databaseComponent=DaggerDatabaseComponent.builder().databaseModule(new DatabaseModule((Application) getApplicationContext())).build();
        databaseComponent.inMyInputMethodService(this);
        databaseComponent.getRealmHelper().createRealm(getApplicationContext());

        KeyboardTheme k=databaseComponent.getRealmHelper().retrieve(id);
        keyboard = new Keyboard(this, R.xml.number_pad);
        keyboardView.setKeyboard(keyboard);
        keyboardView.setOnKeyboardActionListener(this);
       // keyboardView.setBackgroundColor(R.color.red);
        imageView=keyboardView.getRootView().findViewById(R.id.image);
        inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        //String imagePath = ;

        /*String background=Environment.
                getExternalStoragePublicDirectory(".keypal").getAbsolutePath() + "/" + k.getBackgroundImg();*/

        if(saveSharePref.getBool("active")) {
            File file = new File(Environment.getExternalStorageDirectory() + "/" + ".keypal" + "/" + image);
            Uri imageUri = Uri.fromFile(file);

            Glide.with(this)
                    .load(imageUri)
                    .into(new SimpleTarget<Drawable>() {

                        @Override
                        public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                            keyboardView.setBackground(resource);
                        }
                    });
        }
        /*Glide.with(this)
                .load(R.drawable.fairy_3585887_960_720)
                .placeholder(R.drawable.background)
                .into(new SimpleTarget<Drawable>() {

                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                        keyboardView.setBackground(resource);
                    }
                });*/

        return keyboardView;

    }

    @Override
    public void onKey(int primaryCode, int[] keyCodes) {
        playSound(primaryCode);
        final InputConnection inputConnection = getCurrentInputConnection();
        if (inputConnection != null) {
            switch(primaryCode) {
                case Keyboard.KEYCODE_DELETE :
                    CharSequence selectedText = inputConnection.getSelectedText(0);
                    //keys.get(primaryCode).icon=R.drawable.ic_backspace_black_24dp;
                    if (TextUtils.isEmpty(selectedText)) {
                        inputConnection.deleteSurroundingText(1, 0);
                    } else {
                        inputConnection.commitText("", 1);
                    }
                    break;
                case Keyboard.KEYCODE_SHIFT:
                    /*MyKeyboardView myKeyboardView=new MyKeyboardView(getApplicationContext(), (AttributeSet) getResources().getDrawable(R.drawable.round_key));
                    myKeyboardView.onDraw();*/
                    if(!modeLanguage) {
                        if (!caps) {
                            keyboard = new Keyboard(this, R.xml.latin_char_shift);
                            caps = true;
                        } else {
                            keyboard = new Keyboard(this, R.xml.number_pad);
                            caps = false;
                        }
                    }
                    else{
                        if (!caps) {
                            keyboard = new Keyboard(this, R.xml.persian_char_shift);
                            caps = true;
                        } else {
                            keyboard = new Keyboard(this, R.xml.persian_num_char);
                            caps = false;
                        }
                    }
                    keyboardView.setKeyboard(keyboard);
                    keyboardView.setOnKeyboardActionListener(this);
                    break;
                case Keyboard.KEYCODE_DONE:
                    inputConnection.sendKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_ENTER));
                    inputConnection.sendKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_ENTER));
                    break;

                case Keyboard.KEYCODE_MODE_CHANGE:
                    if(!modeLanguage) {
                        if (!caps) {
                            if (!mode) {
                                keyboard = new Keyboard(this, R.xml.symbol_pad);
                                mode = true;
                            } else {
                                keyboard = new Keyboard(this, R.xml.number_pad);
                                mode = false;
                            }
                        }
                        else{
                            if (!mode) {
                            keyboard = new Keyboard(this, R.xml.symbol_pad);
                            mode = true;
                        } else {
                            keyboard = new Keyboard(this, R.xml.latin_char_shift);
                            mode = false;
                        }

                        }
                    }
                    else{
                        if(!caps) {
                            if (!mode) {
                                keyboard = new Keyboard(this, R.xml.persian_symbol_pad);
                                mode = true;
                            } else {
                                keyboard = new Keyboard(this, R.xml.persian_num_char);
                                mode = false;
                            }
                        }
                        else{
                            if (!mode) {
                                keyboard = new Keyboard(this, R.xml.persian_symbol_pad);
                                mode = true;
                            } else {
                                keyboard = new Keyboard(this, R.xml.persian_char_shift);
                                mode = false;
                            }
                        }
                    }
                    keyboardView.setKeyboard(keyboard);
                    keyboardView.setOnKeyboardActionListener(this);
                    break;
                case -102:
                    /*EmoticonGIFKeyboardFragment.EmoticonConfig emoticonConfig = new EmoticonGIFKeyboardFragment.EmoticonConfig()
                            .setEmoticonProvider(IosEmoticonProvider.create());*/
//***********************popup.showAtBottomPending();
                    LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
                    View popupView = layoutInflater.inflate(R.layout.emojicons, null);
                    popupWindow = new EmojiconsPopup(popupView, this);
                    // final PopupWindow popupWindow = new PopupWindow();
                    popupWindow.setSizeForSoftKeyboard();
                    popupWindow.setSize(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
                    popupWindow.showAtLocation(keyboardView.getRootView(), Gravity.BOTTOM, 0, 0);

                    // Bring soft keyboard up : NOT WORKING
                    final InputMethodManager mInputMethodManager = (InputMethodManager) getBaseContext()
                            .getSystemService(Context.INPUT_METHOD_SERVICE);

                    mInputMethodManager.showSoftInput(popupView, 0);


                    // If the text keyboard closes, also dismiss the emoji popup
                    popupWindow.setOnSoftKeyboardOpenCloseListener(new EmojiconsPopup.OnSoftKeyboardOpenCloseListener() {

                        @Override
                        public void onKeyboardOpen(int keyBoardHeight) {

                        }

                        @Override
                        public void onKeyboardClose() {
                            if (popupWindow.isShowing())
                                popupWindow.dismiss();
                        }
                    });
                    popupWindow.setOnEmojiconClickedListener(new EmojiconGridView.OnEmojiconClickedListener() {

                        @Override
                        public void onEmojiconClicked(Emojicon emojicon) {
                                inputConnection.commitText(emojicon.getEmoji(),  inputConnection.getTextBeforeCursor(Character.MAX_VALUE, 0).length());
                        }
                    });

                    popupWindow.setOnEmojiconBackspaceClickedListener(new EmojiconsPopup.OnEmojiconBackspaceClickedListener() {

                        @Override
                        public void onEmojiconBackspaceClicked(View v) {
                            CharSequence selectedText = inputConnection.getSelectedText(0);
                            //keys.get(primaryCode).icon=R.drawable.ic_backspace_black_24dp;
                            if (TextUtils.isEmpty(selectedText)) {
                                inputConnection.deleteSurroundingText(1, 0);
                            } else {
                                inputConnection.commitText("", 1);
                            }
                        }
                    });


                    break;
                case -101:

                    if(!modeLanguage){
                        keyboard=new Keyboard(this,R.xml.persian_num_char);
                        modeLanguage=true;
                    }
                    else {
                        keyboard = new Keyboard(this, R.xml.number_pad);
                        modeLanguage=false;
                    }
                    keyboardView.setKeyboard(keyboard);
                    keyboardView.setOnKeyboardActionListener(this);
                    break;
                case 3:

                    if(!modeLanguage) {
                        if(!modeSym){
                            keyboard=new Keyboard(this,R.xml.symbol_pad2);
                            modeSym=true;
                        }
                        else {
                            keyboard = new Keyboard(this, R.xml.symbol_pad);
                            modeSym=false;
                        }
                    }
                    else{
                        if(!modeSym){
                            keyboard=new Keyboard(this,R.xml.persian_symbol_pad2);
                            modeSym=true;
                        }
                        else {
                            keyboard = new Keyboard(this, R.xml.persian_symbol_pad);
                            modeSym=false;
                        }
                    }
                    keyboardView.setKeyboard(keyboard);
                    keyboardView.setOnKeyboardActionListener(this);
                    break;
                default :
                    char code = (char) primaryCode;
                    if(Character.isLetter(code) && caps){
                        code = Character.toUpperCase(code);
                    }
                    inputConnection.commitText(String.valueOf(code), 1);

            }
        }

    }

    private void playSound(int keyCode){
        AudioManager am = (AudioManager)getSystemService(AUDIO_SERVICE);
        float vol = (float) 0.5;
        switch(keyCode){
            case 32:
                am.playSoundEffect(AudioManager.FX_KEYPRESS_SPACEBAR, vol);
                break;
            case Keyboard.KEYCODE_DONE:
            case 10:
                am.playSoundEffect(AudioManager.FX_KEYPRESS_RETURN, vol);
                break;
            case Keyboard.KEYCODE_DELETE:
                am.playSoundEffect(AudioManager.FX_KEYPRESS_DELETE, vol);
                break;
            default: am.playSoundEffect(AudioManager.FX_KEYPRESS_STANDARD, vol);
        }
    }

    @Override
    public void onPress(int primaryCode) {
        keyboardView.setPreviewEnabled(false);
    }

    @Override
    public void onRelease(int primaryCode) { }

    @Override
    public void onText(CharSequence text) { }

    @Override
    public void swipeLeft() { }

    @Override
    public void swipeRight() { }

    @Override
    public void swipeDown() { }

    @Override
    public void swipeUp() { }

    private void changeEmojiKeyboardIcon(ImageView iconToBeChanged, int drawableResourceId){
        iconToBeChanged.setImageResource(drawableResourceId);
    }

}