package com.keypal.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.keypal.model.IMessageListener;
import com.keypal.model.KeyboardTheme;
import com.keypal.webservice.WebserviceCaller;

import java.util.ArrayList;
import java.util.List;

public class Slider implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {

    ArrayList urlPick,id;
    Context context;
    KeyboardTheme keyboardTheme;
    WebserviceCaller webserviceCaller;

    public void addSlider(Context context, SliderLayout sliderShow){
        this.context=context;
        urlPick = new ArrayList();
        id = new ArrayList();
        id.add("1");
        id.add("2");
        id.add("3");
        urlPick.add("http://www.androidforum.site/diamond-keyboard/keyboard/images/Screenshot_20190530-213849.png");
        urlPick.add("http://www.androidforum.site/diamond-keyboard/keyboard/images/Screenshot_20190530-211311.png");
        urlPick.add("http://www.androidforum.site/diamond-keyboard/keyboard/images/Screenshot_20190530-212504.png");

        for (int i =0;i<urlPick.size();i++) {
            TextSliderView textSliderView = new TextSliderView(context);
            textSliderView.image((String) urlPick.get(i)).setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);

            textSliderView.bundle(new Bundle());
            textSliderView.getBundle().putString("extra", (String) id.get(i));
            sliderShow.addSlider(textSliderView);

        }

    }



    @Override
    public void onSliderClick(BaseSliderView slider) {
        webserviceCaller=new WebserviceCaller();

        switch (slider.getBundle().getString("extra")){
            case "1":
                webserviceCaller.getSlider(11, 24, new IMessageListener() {
                    @Override
                    public void onSuccessObject(Object response) {

                    }

                    @Override
                    public void onSuccess(List response) {
                        Log.e("","");
                        DownloadSlider cdd=new DownloadSlider(context, response);
                        cdd.show();
                    }

                    @Override
                    public void onError(String errorMessage) {
                        Log.e("","");
                        //Toast.makeText(context,errorMessage,Toast.LENGTH_SHORT).show();
                    }
                });
                break;
            case "2":
                /*Intent intent_new=new Intent(context, GetWallpaperByCatActivity.class);
                intent_new.putExtra("category","3");
                context.startActivity(intent_new);*/
                webserviceCaller.getSlider(11, 23, new IMessageListener() {
                    @Override
                    public void onSuccessObject(Object response) {

                    }

                    @Override
                    public void onSuccess(List response) {
                        Log.e("","");
                        DownloadSlider cdd=new DownloadSlider(context, response);
                        cdd.show();
                    }

                    @Override
                    public void onError(String errorMessage) {
                        Log.e("","");
                        //Toast.makeText(context,errorMessage,Toast.LENGTH_SHORT).show();
                    }
                });
                //Toast.makeText(context,slider.getBundle().getString("extra"),Toast.LENGTH_SHORT).show();

                break;
            case "3":
                webserviceCaller.getSlider(11, 21, new IMessageListener() {
                    @Override
                    public void onSuccessObject(Object response) {

                    }

                    @Override
                    public void onSuccess(List response) {
                        Log.e("","");
                        DownloadSlider cdd=new DownloadSlider(context, response);
                        cdd.show();
                    }

                    @Override
                    public void onError(String errorMessage) {
                        Log.e("","");
                        //Toast.makeText(context,errorMessage,Toast.LENGTH_SHORT).show();
                    }
                });
                //Toast.makeText(context,slider.getBundle().getString("extra"),Toast.LENGTH_SHORT).show();

                break;

        }
    }

    @Override
    public void onPageScrolled(int position, float offset, int offsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
