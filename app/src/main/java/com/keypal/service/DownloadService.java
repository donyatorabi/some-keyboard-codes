package com.keypal.service;

import android.app.Application;
import android.app.DownloadManager;
import android.app.IntentService;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.view.View;
import android.widget.ProgressBar;

import com.keypal.MainActivity;
import com.keypal.R;
import com.keypal.di.DaggerDatabaseComponent;
import com.keypal.di.DatabaseComponent;
import com.keypal.di.DatabaseModule;
import com.keypal.fragment.HomeFragment;
import com.keypal.model.KeyboardTheme;
import com.keypal.ui.DownloadDialog;
import com.keypal.webservice.ApiClient;

import java.util.zip.Inflater;

public class DownloadService extends IntentService {
    private static final String DOWNLOAD_PATH = "com.keypal.service_DownloadService_Download_path";
    private static final String DOWNLOAD_PATH_BACKGROUND = "com.keypal.service_DownloadService_DOWNLOAD_PATH_BACKGROUND";
    private static final String DOWNLOAD_PATH_BACKSPACE = "com.keypal.service_DownloadService_DOWNLOAD_PATH_BACKSPACE";
    private static final String DOWNLOAD_PATH_LANGUAGE = "com.keypal.service_DownloadService_DOWNLOAD_PATH_LANGUAGE";
    private static final String DOWNLOAD_PATH_SPACEBAR = "com.keypal.service_DownloadService_DOWNLOAD_PATH_SPACEBAR";
    private static final String DOWNLOAD_PATH_ENTER = "com.keypal.service_DownloadService_DOWNLOAD_PATH_ENTER";
    private static final String DOWNLOAD_PATH_EMOJI = "com.keypal.service_DownloadService_DOWNLOAD_PATH_EMOJI";
    private static final String DOWNLOAD_PATH_SHIFT = "com.keypal.service_DownloadService_DOWNLOAD_PATH_SHIFT";
    String screenshot,background,backspace,language,spacebar,enter,emoji,shift,title
            ,id,key_id,start_color,center_color,end_color,stroke_width,stroke_color,corner_radius,text_color, hour, min, des;
    DatabaseComponent databaseComponent;
    KeyboardTheme keyboardTheme;
    View view;
    public DownloadService() {
        super("DownloadService");
    }
    public static Intent getDownloadService(final  Context callingClassContext, final  String downloadPath
            , final String downloadBackground, final String downloadBackspace, final String downloadLanguage
            , final String downloadSpacebar, final String downloadEnter, final String downloadEmoji, final String downloadShift) {
        return new Intent(callingClassContext, DownloadService.class)
                .putExtra(DOWNLOAD_PATH, downloadPath)
                .putExtra(DOWNLOAD_PATH_BACKGROUND,downloadBackground)
                .putExtra(DOWNLOAD_PATH_BACKSPACE,downloadBackspace)
                .putExtra(DOWNLOAD_PATH_LANGUAGE,downloadLanguage)
                .putExtra(DOWNLOAD_PATH_SPACEBAR,downloadSpacebar)
                .putExtra(DOWNLOAD_PATH_ENTER,downloadEnter)
                .putExtra(DOWNLOAD_PATH_EMOJI,downloadEmoji)
                .putExtra(DOWNLOAD_PATH_SHIFT,downloadShift);

    }
    @Override
    protected void onHandleIntent( Intent intent) {
        String downloadPath = intent.getStringExtra(DOWNLOAD_PATH);
        String downloadPathBackground = intent.getStringExtra(DOWNLOAD_PATH_BACKGROUND);
        String downloadPathBackspace = intent.getStringExtra(DOWNLOAD_PATH_BACKSPACE);
        String downloadPathBLanguage = intent.getStringExtra(DOWNLOAD_PATH_LANGUAGE);
        String downloadPathSpacebar = intent.getStringExtra(DOWNLOAD_PATH_SPACEBAR);
        String downloadPathEnter = intent.getStringExtra(DOWNLOAD_PATH_ENTER);
        String downloadPathEmoji = intent.getStringExtra(DOWNLOAD_PATH_EMOJI);
        String downloadPathShift = intent.getStringExtra(DOWNLOAD_PATH_SHIFT);
        screenshot=intent.getStringExtra("screenshot");
        title=intent.getStringExtra("title");
        background=intent.getStringExtra("background");
        backspace=intent.getStringExtra("backspace");
        language=intent.getStringExtra("language");
        spacebar=intent.getStringExtra("spacebar");
        enter=intent.getStringExtra("enter");
        emoji=intent.getStringExtra("emoji");
        shift=intent.getStringExtra("shift");
        id=intent.getStringExtra("id");
        key_id=intent.getStringExtra("key_id");
        start_color=intent.getStringExtra("start_color");
        center_color=intent.getStringExtra("center_color");
        end_color=intent.getStringExtra("end_color");
        stroke_width=intent.getStringExtra("stroke_width");
        stroke_color=intent.getStringExtra("stroke_color");
        corner_radius=intent.getStringExtra("corner_radius");
        text_color=intent.getStringExtra("text_color");
        des=intent.getStringExtra("des");
        startDownload(downloadPath,des);
        startDownload(downloadPathBackground,des);
        startDownload(downloadPathBackspace,des);
        startDownload(downloadPathBLanguage,des);
        startDownload(downloadPathSpacebar,des);
        startDownload(downloadPathEnter,des);
        startDownload(downloadPathEmoji,des);
        startDownload(downloadPathShift,des);
        daggerRealmSave(getApplication());
    }
    private void startDownload(String downloadPath, String destinationPath) {
        Uri uri = Uri.parse(downloadPath); // Path where you want to download file.
        DownloadManager.Request request = new DownloadManager.Request(uri);
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE | DownloadManager.Request.NETWORK_WIFI);  // Tell on which network you want to download file.
        request.setVisibleInDownloadsUi(true);
        request.setDestinationInExternalPublicDir(destinationPath, uri.getLastPathSegment());  // Storage directory path
        /*final ProgressDialog progressDoalog;
        progressDoalog = new ProgressDialog(getApplicationContext());
        progressDoalog.setMax(100);
        progressDoalog.setMessage("Its loading....");
        progressDoalog.setTitle("ProgressDialog bar example");
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        // show it
        progressDoalog.show();
*/

        //((DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE)).enqueue(request); // This will start downloading
        final DownloadManager manager = ((DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE));
        final long downloadId = manager.enqueue(request);
    }

    public void daggerRealmSave(Application application){

        com.keypal.model.KeyboardTheme f=new com.keypal.model.KeyboardTheme();
        f.setId(id);
        f.setTitle(title);
        f.setBackgroundImg(background);
        f.setScreenshotImg(screenshot);
        f.setBackspace(backspace);
        f.setLanguage(language);
        f.setSpaceBar(spacebar);
        f.setEnter(enter);
        f.setEmoji(emoji);
        f.setShift(shift);
        f.setKeyId(key_id);
        f.setStartColor(start_color);
        f.setCenterColor(center_color);
        f.setEndColor(end_color);
        f.setStrokeWidth(stroke_width);
        f.setStrokeColor(stroke_color);
        f.setCornerRadius(corner_radius);
        f.setTextColor(text_color);

        databaseComponent=DaggerDatabaseComponent.builder().databaseModule(new DatabaseModule(application)).build();
        databaseComponent.inService(this);
        databaseComponent.getRealmHelper().createRealm(getApplicationContext());

        databaseComponent.getRealmHelper().save(f);

    }
}
